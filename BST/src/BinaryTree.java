
public class BinaryTree {
	Node root;
	Node previous; //previous Node
	
	boolean isBinarySearchTree(){
		this.previous = null;
		
		return isBinarySearchTree(root);
		
	}

     boolean isBinarySearchTree(Node node) {
    	 if (node != null){
    		 
    	 }
    	 
    		 
    	  if (!isBinarySearchTree(node.left))
    		 return false;
    	 
    	 else if (previous != null && node.data <= previous.data)
    		 return false;
    	 previous = node;
    	 return isBinarySearchTree(node.right);
    	 
    	 }
     

	public static void main(String[] args) {
	BinaryTree searchTree = new BinaryTree();
	searchTree.root = searchTree.new Node(14);
	searchTree.root.left = searchTree.new Node(19);
	searchTree.root.right = searchTree.new Node(24);
	searchTree.root.left.left = searchTree.new Node(43);
	searchTree.root.left.right = searchTree.new Node(63);
	
	if ( searchTree.isBinarySearchTree())
		System.out.println("The tree is Binary Search Tree. ");
	else
		System.out.println("The tree is not a Binary Search Tree. ");
		

	}
class Node{
	int data;
	Node left;
	Node right;
	
	public Node(int item){
		this.data = item;
		this.left = null;
		this.right = null;
		
	}
}
}
