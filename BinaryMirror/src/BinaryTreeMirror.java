
public class BinaryTreeMirror {
	Node root;
	
	public void mirror(){
		root = mirror(root);
		
	}
	Node mirror(Node node){
		
		if (node == null)
			return node;
		Node left = mirror(node.left);
		Node right = mirror(node.right);
		
		node.left = right;
		node.right = left;
		
		return node;
	}
public void inOrder(){
	inOrder(root);
}
public void inOrder(Node node){
	if (node == null)
		return;
	
	inOrder(node.left);
	System.out.print(node.data + " ");
	inOrder(node.right);
}
	public static void main(String[] args) {

		 BinaryTreeMirror mirrorTree = new BinaryTreeMirror();
		 mirrorTree.root = new Node(11);
		 mirrorTree.root.left = new Node(12);
		 mirrorTree.root.right = new Node(13);
		 mirrorTree.root.left.left = new Node(14);
		 mirrorTree.root.left.right = new Node(15);
		 
		 System.out.println("The inOrder process of the Binary tree is: ");
		 mirrorTree.inOrder(); 
		 System.out.println();
		 
		 mirrorTree.mirror();// makes a mirror of the tree
		 System.out.println("The inOrder process of the mirror tree is: ");
		 mirrorTree.inOrder();
		 
	}
	
	}
 class Node{
	Node left;
	Node right;
	int data;
	
	public Node (int item){
		data = item;
		left = null;
		right = null;
		
	}
}
