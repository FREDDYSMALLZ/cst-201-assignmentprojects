import java.util.Enumeration;
import java.util.Hashtable;
public class HashTables {

	public static void main(String[] args) {
		Hashtable myGirlFriends = new Hashtable();
		Enumeration namesAndBalance;
		double balance;
		String str;
		
		myGirlFriends.put("Nancy",new Double (120.5));
		myGirlFriends.put("Mary",new Double (140.5));
		myGirlFriends.put("Esther",new Double (150.5));
		myGirlFriends.put("Judy",new Double (160.5));
		myGirlFriends.put("Elise",new Double (1220.5));
		myGirlFriends.put("Annalise",new Double (1320.5));
		myGirlFriends.remove("Esther");
		
		namesAndBalance = myGirlFriends.keys();
		while( namesAndBalance.hasMoreElements()){
		str = (String) namesAndBalance.nextElement();
		System.out.println(str + ": " + myGirlFriends.get(str));
		}
		
		// Deposit 25000 into Judy's account
		 balance = ((Double)myGirlFriends.get("Judy")).doubleValue();
	     myGirlFriends.put("Judy", new Double (balance + 2500));
	      System.out.println("Judy's new balance: " + myGirlFriends.get("Judy"));
	      
	      //Deduct 1000 from Elise
	      balance = ((Double)myGirlFriends.get("Elise")).doubleValue();
	      myGirlFriends.put("Elise", new Double (balance - 1000));
	      System.out.println("Elise's new balance is: " + myGirlFriends.get("Elise"));
	   }
	
	}


