import java.util.HashSet;

public final class  PhoneNumber {
	private final int area;   // area code the first 3 digits
    private final int exch;   // exchange  the next 3 digits
    private final int ext;    // extension the last 4 digits
    
    public PhoneNumber(int area, int exch, int ext) {
        this.area = area;
        this.exch = exch;
        this.ext  = ext;
    }
  
    public boolean equals(Object other) {
        if (other == this) return true;
        if (other == null) return false;
        if (other.getClass() != this.getClass()) return false;
        PhoneNumber that = (PhoneNumber) other;
        return (this.area == that.area) && 
        		(this.exch == that.exch) && (this.ext == that.ext);
    }
  
    public String toString() {
                return String.format("(%03d) %03d-%04d", area, exch, ext);
    }
   
    public int hashCode() {
        return 31 * (area + 31 * exch) + ext;
    }
    
	public static void main(String[] args) {
		PhoneNumber John = new PhoneNumber(817, 228, 4415);
        PhoneNumber Wallace = new PhoneNumber(219, 886, 5300);
        PhoneNumber Charles = new PhoneNumber(409, 255, 5339);
        PhoneNumber Thomas = new PhoneNumber(215, 876, 2009);
        PhoneNumber Esther = new PhoneNumber(817, 306, 3308);
        PhoneNumber Francis = new PhoneNumber(214, 235, 2690);
        PhoneNumber George = new PhoneNumber(817, 223, 3302);
        PhoneNumber Henry = new PhoneNumber(289, 345, 4536);
        PhoneNumber Frankline = new PhoneNumber(817, 227, 2700);
        PhoneNumber Fredrick = new PhoneNumber(214, 435, 6550);
        PhoneNumber Rodgers = new PhoneNumber(467, 445, 4430);
        
        System.out.println("John = " + John);
        System.out.println("Wallace = " + Wallace);
        System.out.println("Charles = " + Charles);
        System.out.println("Thomas = " + Thomas);
        System.out.println("Esther = " + Esther);
        System.out.println("Francis = " + Francis);
        System.out.println("George = " + George);
        System.out.println("Henry = " + Henry);
        System.out.println("Frankline = " + Frankline);
        System.out.println("Fredrick = " + Fredrick);
        System.out.println("Rodgers = " + Rodgers);
        

        HashSet<PhoneNumber> set = new HashSet<PhoneNumber>();
        set.add(Rodgers); //adds data into the hash table
        set.add(Fredrick);
        set.add(Frankline);
        set.add(Henry);
        set.add(George);
        set.add(Esther);
        set.add(Charles);
        set.add(Wallace);
        set.add(Francis);
        set.add(Thomas);
        set.add(John);
        set.remove(Fredrick); //Removes an entry from the table
        System.out.println("The removed entry is: " + set.remove(Fredrick));
      
        System.out.println("Rodgers: " + set.contains(Rodgers));
        System.out.println("Fredrick: " + set.contains(Fredrick));
        System.out.println("Frankline:  " + set.contains(Frankline));
        System.out.println("Henry:  " + set.contains(Henry));
        System.out.println("George:  " + set.contains(George));
        System.out.println("Esther: " + set.contains(Esther));
        System.out.println("Charles: " + set.contains(Charles));
        System.out.println("Wallace: " + set.contains(Wallace));
        System.out.println("Francis: " + set.contains(Francis));
        System.out.println("Thomas: " + set.contains(Thomas));
        System.out.println("John: " + set.contains(John));
    
	}

}
