import java.util.LinkedList;
public class doubleLinkedList {

	public static void main(String[] args) {
		LinkedList<String> doublyLinkedList = new LinkedList<>();
		
		doublyLinkedList.add("DATSUN");
		doublyLinkedList.add("TOYOTA");
		doublyLinkedList.add("MERCEDES");
		doublyLinkedList.add("FORD");
		doublyLinkedList.add("HONDA");
		doublyLinkedList.add("SUZUKI");
		doublyLinkedList.add("CHEVROLET");
		doublyLinkedList.removeLast();
		doublyLinkedList.add(4,"NISSAN");
		doublyLinkedList.addFirst("PASSAT");
		doublyLinkedList.removeFirst();
		
		System.out.println(doublyLinkedList.toString());

	}
	class Node<D>{
		Node next;
		Node prev;
		D data;
		
		public Node(D data) {
            this.data = data;
        }

        public void displayNode() {
           
        }

        @Override
        public String toString() {
            return data.toString();
        }
    }

    public Node first = null;
    public Node last = null;

    public <D> void addFirst(D data) {
        Node newNode = new Node(data);

        if (isEmpty()) {
            newNode.next = null;
            newNode.prev = null;
            first = newNode;
            last = newNode;
        } 
        else {
            first.prev = newNode;
            newNode.next = first;
            newNode.prev = null;
            first = newNode;
        }
    }

    public boolean isEmpty() {
        return (first == null);
    }

    public void displayList() {
        Node current = first;
        while (current != null) {
            current.displayNode();
            current = current.next;
        }
    }

    public void removeFirst() {
        if (!isEmpty()) {
            if (first.next == null) {
                first = null;
                last = null;
            } else {
                first = first.next;
                first.prev = null;
            }
        }
    }
    public void removeLast() {
        if (!isEmpty()) {

            if (first.next == null) {
                first = null;
                last = null;
            } 
            else {
                last = last.prev;
                last.next = null;
            }
        }     
    }
}
