import java.util.Scanner;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Queue;

public class doublePalindrome {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		//Promt the user to enter a string line of characters
		
		String line = input.nextLine();
		 System.out.print("Please enter your word: ");
		 line = input.nextLine( );
		 if (is_palindrome(line))
			    System.out.println("That is a palindrome word:");
			 else
			    System.out.println("That is not a palindrome word:");
		      
		      while (line.length() != 0);
		   }

	private static boolean is_palindrome(String input) {
		Queue<Character> q = new LinkedList<Character>();
		Stack<Character> s = new Stack<Character>();
	      Character letter;   
	      int mismatches = 0;
	      int i;              
	      
	      for (i = 0; i < input.length( ); i++) {
		 letter = input.charAt(i);
		 
	         if (Character.isLetter(letter)) {
	            q.add(letter);
	            s.push(letter);
	         }
	      }
	      
	      while (!q.isEmpty( )) {
	         if (q.remove( ) != s.pop( ))
	            mismatches++;
	      }

	      return (mismatches == 0); 
	   }
	    
	}
	



