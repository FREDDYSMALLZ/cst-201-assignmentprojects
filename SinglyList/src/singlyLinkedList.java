import java.util.LinkedList;
import java.util.Collections;

public class singlyLinkedList {

	public static void main(String[] args) {
		LinkedList<Integer> mySinglylist = new LinkedList<>();
		LinkedList<String> singleList = new LinkedList<>();
		LinkedList<String> list = new LinkedList<>();
		
		singleList.push("Mango");
		singleList.push("Bananas");
		singleList.add("PineApples");
		
		mySinglylist.push(11);
        mySinglylist.push(12);
        mySinglylist.push(13);
        mySinglylist.push(14);
        mySinglylist.push(15);
        mySinglylist.push(16);
        
        mySinglylist.remove(0);//Removes the node at 0
        mySinglylist.add(4, 20);//Adds an element at the specified position
        Collections.copy(mySinglylist, mySinglylist);//makes a copy of the list
        Collections.reverse(mySinglylist);//Reverses the order of the list
       
        
        
        System.out.println("The order of the list is:" + mySinglylist);
        System.out.println("The order of the list after" +
        " removing integer at 0 is: " + mySinglylist);
        System.out.println("The order of the list after" +
                " adding an integer at 4 is: " + mySinglylist);
        System.out.println("The order of the list is: " +
                mySinglylist + " after making a copy.");
        System.out.println("This is the reverse order" + " of "
        		+ " the list " + mySinglylist);
        System.out.println("The second list is: " + list);
		
	}
class mySinglylist{
	Node head;
}
class singleList{
	Node head;
}
class list{
	Node head;
}
class Node{
	int data;
	Node next;
	Node(int d){
		data = d;
		next = null;
	}
	
}

private Node head;

public void push(int new_data){
   
    Node newnode = new Node(new_data);

    newnode.next = head;

    head = newnode;
}

void deleteNode(int position){
        if (head == null)
        return;

    Node temp = head;

    if (position == 0){
        head = temp.next;   
        return;
    }

    for (int i=0; temp!=null && i<position-1; i++)
        temp = temp.next;

    if (temp == null || temp.next == null)
        return;

    Node next = temp.next.next;

    temp.next = next;  
}

public void printList(){
    Node tnode = head;
    while (tnode != null){
        System.out.print(tnode.data+" ");
        tnode = tnode.next;
    }
}
}

