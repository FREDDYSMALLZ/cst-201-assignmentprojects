import java.util.ArrayList;
import java.util.List;
import javax.swing.tree.TreeNode;
public class TraversingBinaryTrees {
	public int value;
    public int left;
    public int right;
 
	public TraversingBinaryTrees(int value, int left,
			int right) {
	
		this.value = value;
		this.left = left;
		this.right = right;
	}

	public List<Integer> preorderTraversal(TreeNode root) {
		
        List<Integer> TraversingBinaryTree = new ArrayList<Integer>();
        TraversingBinaryTree.add(12);
        TraversingBinaryTree.add(14);
        TraversingBinaryTree.add(16);
        TraversingBinaryTree.add(18);
        TraversingBinaryTree.add(20);
        
        
        if (root != null){
        	
            TraversingBinaryTree.add(root.getChildCount());
            TraversingBinaryTree.addAll(preorderTraversal(root.getChildAt(left)));
            TraversingBinaryTree.addAll(preorderTraversal(root.getChildAt(right)));
        }
        return TraversingBinaryTree;
    }

	public static void main(String[] args) {

System.out.println("The preOrder of traversing the Binary tree is:" );

	}

}
